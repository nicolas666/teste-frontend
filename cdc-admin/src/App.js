import React, { Component } from 'react';
import './css/pure-min.css';
import './css/side-menu.css';
import $ from 'jquery';


class App extends Component {

  // Constructor é onde tudo que é dinâmico //
  //é declarado e onde tudo pode ser gerenciado //
  constructor(){
    super();
    this.state = {lista: [], name:'', price_usd:'', price_btc:'', symbol:'', search:''};
  }

  // updatedSearch é responsável por atualizar a //
  // let filteredCoins que carrega a llista criada no constructor //
  updateSearch(event){
    this.setState({search: event.target.value.substr(0,
          20)})
    console.log(event);
  }

  // Ajax que chama a API //
  componentDidMount(){
    $.ajax({
      url:"https://api.coinmarketcap.com/v1/ticker/",
      dataType: 'json',
      success:function(resposta){
        this.setState({lista:resposta});
      }.bind(this)
    });
  };

// Render é responsável por montar o Component principal do sistema //
render() {

  // let criado para simplificar a manipulação dos dados na lista //
let filteredCoins = this.state.lista.filter(
  (coin) => {
    return coin.name.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1;
  }
);

return (
<div id="layout">

    <a href="#menu" id="menuLink" className="menu-link">

        <span></span>
    </a>

    <div id="menu">
        <div className="pure-menu">
            <a className="pure-menu-heading" href="./App">Market</a>

            <ul className="pure-menu-list">
                <li className="pure-menu-item, pure-menu-link" href="./App">CAP</li>
            </ul>
        </div>
    </div>

        <div id="main">
            <div className="header">
              <h1>Test Front</h1>
            </div>
            <div className="content" id="content">
              <div className="pure-form pure-form-aligned">
                <div className="pure-control-group">
                  <label htmlFor="nome">Search</label>
                  <input type="search" 
                    value={this.state.search} 
                    onChange={this.updateSearch.bind(this)}/>
                </div>
              </div>
              <div>
                <table className="pure-table">
                  <thead>
                    <tr>
                      <th>Symbol</th>
                      <th>Nome</th>
                      <th>Value in USD</th>
                      <th>Value in BTC</th>
                    </tr>
                  </thead>
                  <tbody>

                    {/* Estrutura principal da lista que é chamada pelo Ajax
                        e estruturada por let filteredCoins. */}
                    {filteredCoins.map((coin) => {
                        return(
                            <tr key={coin.symbol}>
                              <td>{coin.symbol}</td>
                              <td>{coin.name}</td>
                              <td>U$ = {coin.price_usd}</td>
                              <td>BTC = {coin.price_btc}</td>
                            </tr>
                          );
                      })
                    }


                  </tbody>
                </table>
              </div>
            </div>
          </div>


</div>
    );
  }
}

export default App;
